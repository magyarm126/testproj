﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if (UNITY_EDITOR)
using UnityEditor;
#endif

/// <summary>
/// Optimize: Resourceloader
/// </summary>

public class UIController : MonoBehaviour {

    //[TextArea]
    public string _input_text;
    public int _content_index;

    private List<GameObject> _panelContents = new List<GameObject>();
    private Dictionary<GameObject, List<GameObject>> _subContents = new Dictionary<GameObject, List<GameObject>>();

    public void Addbutton(GameObject go = null)
    {
        if (go == null)
        {
            if (_content_index < 0 || _content_index >= _panelContents.Count)
                return;
            GameObject parent = _panelContents[_content_index];
            GameObject button =  Instantiate(Resources.Load<GameObject>("Prefabs/UI/SampleButton"), parent.transform.GetChild(0).GetChild(0).GetChild(0)); //SampleScroll/ViewPort/Content/Panel

            var buttoncompontent = button.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>();
            if(buttoncompontent)
            {
                buttoncompontent.text = _input_text;
            }

            _subContents[parent].Add(button);
        }
        else
        {
            Instantiate(Resources.Load<GameObject>("Prefabs/UI/SampleButton"), go.transform); //SampleScroll/ViewPort/Content/Panel
            //Fix : Can't acces it after instantiating
        }
    }
    public void AddScrollView()
    {
        GameObject tmp = Instantiate(Resources.Load<GameObject>("Prefabs/UI/SampleScroll"), this.transform);
        _panelContents.Add(tmp);
        _subContents.Add(tmp, new List<GameObject>());
    }
    public void DeleteAll()
    {
        foreach(var content in _panelContents)
        {
            if(content != null)
            DestroyImmediate(content.gameObject);
        }
        _panelContents.Clear();
        _subContents.Clear();

        while(transform.childCount>1)
        {//Only use if the content is empty by default
            DestroyImmediate(transform.GetChild(1).gameObject);
        }
    }
}
#if (UNITY_EDITOR)
[CustomEditor(typeof(UIController))]
public class ObjectBuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        UIController myScript = (UIController)target;

        if (GUILayout.Button("Add button"))
        {
            myScript.Addbutton();
        }
        if (GUILayout.Button("Add ScrollView"))
        {
            myScript.AddScrollView();
        }
        if (GUILayout.Button("Delete all"))
        {
            myScript.DeleteAll();
        }
    }
}
#endif