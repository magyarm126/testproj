﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Optimize: array instead of string
/// </summary>

public class PlayerController : MonoBehaviour {

    public bool leftButtoon;
    public bool rightButtoon;
    public bool upButtoon;
    public bool downButtoon;

    public void VirtualInputStopped()
    {
        leftButtoon = rightButtoon = upButtoon = downButtoon = false;
    }

    public void VirutalButtonPressed(string but)
    {
        if (but == "left")
        {
            leftButtoon = true;
        }
        if (but == "right")
        {
            rightButtoon = true;
        }
        if (but == "up")
        {
            upButtoon = true;
        }
        if (but == "down")
        {
            downButtoon = true;
        }
    }

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        float speed_dampening = 0.1f;

        Vector3 tba = Vector3.zero;
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || leftButtoon)
        {
            tba = Vector3.left;
        }
        if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D) || rightButtoon)
        {
            tba = Vector3.right;
        }
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) || upButtoon)
        {
            tba = Vector3.up;
        }
        if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S) || downButtoon)
        {
            tba = Vector3.down;
        }
        transform.position += tba * speed_dampening;
    }
}
